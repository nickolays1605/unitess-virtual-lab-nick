﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="calibration_multimetr" Type="Folder" URL="..">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="График неопределённости калибратора.ctl" Type="VI" URL="../../../../lib/ctl/График неопределённости калибратора.ctl"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="3 UPL Server Init.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/3 UPL Server Init.vi"/>
			<Item Name="4 UPL Server Cntr Regist.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/4 UPL Server Cntr Regist.vi"/>
			<Item Name="5 UPL Server Start.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/5 UPL Server Start.vi"/>
			<Item Name="6 UPL Server Event DONE.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/6 UPL Server Event DONE.vi"/>
			<Item Name="DrLL_FormatStringValue.vi" Type="VI" URL="../../../../../../Unitess from another PC/UniTesS Driver/DriverLowLevel/DrLL_FormatStringValue.vi"/>
			<Item Name="IS_EXE.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/IS_EXE.vi"/>
			<Item Name="Postfix2expV2_point(SubVI).vi" Type="VI" URL="../../../../../../Unitess from another PC/UniTesS Driver/Auxiliary/Postfix2expV2_point(SubVI).vi"/>
			<Item Name="RemoveBadSymbols (SubVI).vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RemoveBadSymbols (SubVI).vi"/>
			<Item Name="Server Connections.ctl" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/Server Connections.ctl"/>
			<Item Name="STR_translit.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/STR_translit.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="UError apply custom error code.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError apply custom error code.vi"/>
			<Item Name="UError clear ignored code.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError clear ignored code.vi"/>
			<Item Name="UError create delimeters.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError create delimeters.vi"/>
			<Item Name="UError format message.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError format message.vi"/>
			<Item Name="UError init.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError init.vi"/>
			<Item Name="UError Load Custom Error File.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError Load Custom Error File.vi"/>
			<Item Name="UError Save 2 file log.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError Save 2 file log.vi"/>
			<Item Name="UError.vi" Type="VI" URL="/C/Unitess/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError.vi"/>
			<Item Name="UPL find control index.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL find control index.vi"/>
			<Item Name="UPL Get Exe Ver.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Get Exe Ver.vi"/>
			<Item Name="UPL Message Get Parsing.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Message Get Parsing.vi"/>
			<Item Name="UPL Message Set Parsing.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Message Set Parsing.vi"/>
			<Item Name="UPL Server Array pars Boolean.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Array pars Boolean.vi"/>
			<Item Name="UPL Server Array pars Digital.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Array pars Digital.vi"/>
			<Item Name="UPL Server Array pars String.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Array pars String.vi"/>
			<Item Name="UPL Server Avalible Item.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Avalible Item.vi"/>
			<Item Name="UPL Server Cntr Check if already exist.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Cntr Check if already exist.vi"/>
			<Item Name="UPL Server Cntr Regist.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Cntr Regist.vi"/>
			<Item Name="UPL Server Core.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Core.vi"/>
			<Item Name="UPL Server create help Arguments.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server create help Arguments.vi"/>
			<Item Name="UPL Server create help.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server create help.vi"/>
			<Item Name="UPL Server Find Cntr Ref.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Find Cntr Ref.vi"/>
			<Item Name="UPL Server Pars Array.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars Array.vi"/>
			<Item Name="UPL Server Pars BOLLEAN.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars BOLLEAN.vi"/>
			<Item Name="UPL Server Pars ComboBox.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars ComboBox.vi"/>
			<Item Name="UPL Server Pars Delete Quotes.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars Delete Quotes.vi"/>
			<Item Name="UPL Server Pars DIGITAL.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars DIGITAL.vi"/>
			<Item Name="UPL Server Pars ENUM.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars ENUM.vi"/>
			<Item Name="UPL Server Pars Message.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars Message.vi"/>
			<Item Name="UPL Server Pars MulticolumnListBox.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars MulticolumnListBox.vi"/>
			<Item Name="UPL Server Pars RadioButton.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars RadioButton.vi"/>
			<Item Name="UPL Server Pars RING.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars RING.vi"/>
			<Item Name="UPL Server Pars SLIDE id21.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars SLIDE id21.vi"/>
			<Item Name="UPL Server Pars TabControl.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars TabControl.vi"/>
			<Item Name="UPL Server Pars TIMESTAMP.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Pars TIMESTAMP.vi"/>
			<Item Name="UPL Server Select Item.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server Select Item.vi"/>
			<Item Name="UPL Server value 2 str array.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str array.vi"/>
			<Item Name="UPL Server value 2 str combox.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str combox.vi"/>
			<Item Name="UPL Server value 2 str digit.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str digit.vi"/>
			<Item Name="UPL Server value 2 str enum.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str enum.vi"/>
			<Item Name="UPL Server value 2 str mclb.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str mclb.vi"/>
			<Item Name="UPL Server value 2 str ring.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str ring.vi"/>
			<Item Name="UPL Server value 2 str tab.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server value 2 str tab.vi"/>
			<Item Name="UPL Server VI Control Get.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server VI Control Get.vi"/>
			<Item Name="UPL Server VI Control Set.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server VI Control Set.vi"/>
			<Item Name="UPL Server VI Sys comand.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL Server VI Sys comand.vi"/>
			<Item Name="UPL TCP read.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL TCP read.vi"/>
			<Item Name="UPL TCP Write Error.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL TCP Write Error.vi"/>
			<Item Name="UPL TCP Write Event Done.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL TCP Write Event Done.vi"/>
			<Item Name="UPL TCP write.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL TCP write.vi"/>
			<Item Name="UPL_Cntr Regist Info.ctl" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_Cntr Regist Info.ctl"/>
			<Item Name="UPL_Control.ctl" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_Control.ctl"/>
			<Item Name="UPL_Controls.ctl" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_Controls.ctl"/>
			<Item Name="upl_core_stm.ctl" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/upl_core_stm.ctl"/>
			<Item Name="UPL_Events_Type.ctl" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_Events_Type.ctl"/>
			<Item Name="UPL_Exp2_Postfix.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_Exp2_Postfix.vi"/>
			<Item Name="UPL_Postfix Parser.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_Postfix Parser.vi"/>
			<Item Name="UPL_RunFile.vi" Type="VI" URL="../../../../lib/UnitessPluginLink.llb/UPL_RunFile.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Unitess Lab Calibration Multimetr" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{A0600C6F-4726-4E9C-809F-229DB6715FE4}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4AFDE42F-5F8D-4198-A165-1447835BF8E3}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{A9F10C55-2E73-4D5F-9259-7286564EBA19}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Unitess Lab Calibration Multimetr</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Unitess Lab Calibration Multimetr</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{933F8D87-9A16-4526-89AD-893830FCD483}</Property>
				<Property Name="Bld_version.build" Type="Int">12</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Unitess Lab Calibration Multimetr.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Unitess Lab Calibration Multimetr/Unitess Lab Calibration Multimetr.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Unitess Lab Calibration Multimetr/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{D2B4E20B-628C-486E-94B4-14E2D9930AD2}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/calibration_multimetr/main_calibration_multimetr.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Unitess Lab Calibration Multimetr</Property>
				<Property Name="TgtF_internalName" Type="Str">Unitess Lab Calibration Multimetr</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 </Property>
				<Property Name="TgtF_productName" Type="Str">Unitess Lab Calibration Multimetr</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9C3A26A6-7044-4F9C-ACCA-5FDDA4BB0CE4}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Unitess Lab Calibration Multimetr.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
